(function ($) {

  // Use jQuery to set up event listeners

  $('#btnLoad').click(function () { $('#showResult').load('show.txt') })
  $('#btnAjax').click(function () { callRestAPI() })
  $('#btnxhr').click(async function () { 
    const j = await jsRequest();
    $('#showResultXHR').html(j);
  })

  // Perform asynchronous HTTP API request with jQuery ajax method

  async function callRestAPI() {
    var root = 'https://jsonplaceholder.typicode.com';
    $.ajax({
      url: root + '/posts/1',
      method: 'GET'
    }).then(function (response) {
      console.log(response)
      $('#showResult').html(response.body)
    })
  }

  // Perform asynchronous HTTP API request with JavaScript XMLHttpRequest object
  //returns { 'type': 'success', 'value': { 'id': , 'joke': } }

  async function jsRequest() {
    return new Promise(function (resolve, reject) {
      const req = new XMLHttpRequest()
      const verb = 'GET'
      const url = 'http://api.icndb.com/jokes/random?limitTo=[nerdy]'
      const async = true

      req.timeout = 3000 // millisecs to respond
      req.onreadystatechange = function (e) {
        if (req.readyState === 4) {
          if (req.status === 200) {
            const obj = JSON.parse(req.response)
            const joke = obj.value.joke
            resolve(joke)
          } else {
            reject(req.status)
          }
        }
      }
      req.ontimeout = function () {
        reject('Error - timed out: ' + req.time)
      }
      req.open(verb, url, async)
      req.send()
    })
  }

})(jQuery)

//http://www.programmableweb.com/news/how-to-build-yahoo-weather-hello-world-application-javascript/how-to/2016/01/25?page=2